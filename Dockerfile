FROM golang:1.13 as builder
ADD . /opt/server
WORKDIR /opt/server
RUN go build -o server .

FROM debian:latest
RUN mkdir -p /opt/server
WORKDIR /opt/server
COPY --from=builder /opt/server/server .
CMD ["./server"]
