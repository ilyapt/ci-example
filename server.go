package main

import "net/http"

func main() {
	http.HandleFunc("/", helloRoute)
	if err := http.ListenAndServe(":80", nil); err != nil {
		panic(err)
	}
}

func helloRoute(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello world"))
}